//
//  BSTabBarController.m
//  BSBuDeJie
//
//  Created by mh on 16/3/31.
//  Copyright © 2016年 BS. All rights reserved.
//

#import "DBTabBarController.h"
#import "DBNavigationController.h"
@interface DBTabBarController ()

@end

@implementation DBTabBarController
#pragma mark - 属性懒加载

#pragma mark - 系统方法
- (void)viewDidLoad {
    [super viewDidLoad];
    //添加子控制器
    [self addChildViewControllers];
}

#pragma mark - 初始化方法
//添加子控制器
- (void)addChildViewControllers{
    [self addToNavigationcontrollerWithVc:[[UIViewController alloc] init] image:@"house_normal" selectedImage:@"house_active" title:@"首页"];
    [self addToNavigationcontrollerWithVc:[[UIViewController alloc] init] image:@"ic_tab_xiaoshi" selectedImage:@"ic_tab_xiaoshi_active" title:@"小事"];
    [self addToNavigationcontrollerWithVc:[[UIViewController alloc] init] image:@"discover_normal" selectedImage:@"discover_active" title:@"书影音"];
    [self addToNavigationcontrollerWithVc:[[UIViewController alloc] init] image:@"quanzi_normal" selectedImage:@"quanzi_active" title:@"圈子"];
    [self addToNavigationcontrollerWithVc:[[UIViewController alloc] init] image:@"icon_tabbar_activity" selectedImage:@"icon_tabbar_activity_active" title:@"我的"];
}

#pragma mark - 自定义方法
- (void)addToNavigationcontrollerWithVc:(UIViewController *)Vc image:(NSString *)imageName selectedImage:(NSString *) selectedImageName title:(NSString *)title{
    [self addChildViewController:[[DBNavigationController alloc] initWithRootViewController:Vc]];
    Vc.title = title;
    Vc.tabBarItem.image = [[UIImage imageNamed:imageName] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    Vc.tabBarItem.selectedImage = [[UIImage imageNamed:selectedImageName] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    Vc.view.backgroundColor = [UIColor colorWithRed:arc4random_uniform(256) / 255.0 green:arc4random_uniform(256) / 255.0 blue:arc4random_uniform(256) / 255.0 alpha:1.0];
}
@end
