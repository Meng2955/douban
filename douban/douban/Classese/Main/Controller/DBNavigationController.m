 //
//  BSNavigationController.m
//  BSBuDeJie
//
//  Created by mh on 16/3/31.
//  Copyright © 2016年 BS. All rights reserved.
//

#import "DBNavigationController.h"
#import "UIBarButtonItem+Extension.h"

@interface DBNavigationController ()<UIGestureRecognizerDelegate>
@end

@implementation DBNavigationController
#pragma mark - 系统方法
//初始化方法    设置所有一次性的设置
+ (void)initialize{
    // 设置整个项目所有UIBarButtonItem的主题样式
    UIBarButtonItem *item = [UIBarButtonItem appearance];
    //普通状态
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    dict[NSForegroundColorAttributeName] = [UIColor orangeColor];
    [item setTitleTextAttributes:dict forState:UIControlStateNormal];
    
    //高亮状态
    NSMutableDictionary *highLightedDict = [NSMutableDictionary dictionary];
    dict[NSForegroundColorAttributeName] = [UIColor redColor];
    [item setTitleTextAttributes:highLightedDict forState:UIControlStateHighlighted];
    
    //不可用状态
    NSMutableDictionary *disDict = [NSMutableDictionary dictionary];
    disDict[NSForegroundColorAttributeName] = [UIColor colorWithRed:154/255.0 green:154/255.0 blue:154/255.0 alpha:0.8];
    [item setTitleTextAttributes:disDict forState:UIControlStateDisabled];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    //全屏侧滑返回手势
    [self addGestureRecognizer];
}

- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated{
    if (self.childViewControllers.count > 0) {//非根控制器
        //设置子控制器
        viewController.hidesBottomBarWhenPushed = YES;
        //阴影
        viewController.navigationItem.leftBarButtonItem = [UIBarButtonItem itemWithTarget:self action:@selector(back) image:[UIImage imageNamed:@"ic_arrow_back"] highImage:[UIImage imageNamed:@"ic_arrow_back_subject"]];
    }
    [super pushViewController:viewController animated:animated];
}
/**
 *  点击返回按钮
 */
- (void)back{
    [self popViewControllerAnimated:YES];
}
/**
 *  更多按钮点击
 */
- (void)moreBtnOnClick{
    [self popToRootViewControllerAnimated:YES];
}

//添加全屏侧滑手势
- (void)addGestureRecognizer{
    UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] initWithTarget:self.interactivePopGestureRecognizer.delegate action:NSSelectorFromString(@"handleNavigationTransition:")];
    pan.delegate = self;
    [self.view addGestureRecognizer:pan];
}

#pragma mark - gestureRecognizer代理方法
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    if (self.childViewControllers.count == 1) {
        return NO;
    }
    return YES;
}
@end
