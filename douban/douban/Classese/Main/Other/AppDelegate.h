//
//  AppDelegate.h
//  douban
//
//  Created by mh on 16/4/8.
//  Copyright © 2016年 DB. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

