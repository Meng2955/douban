//
//  UIView+MHFrame.m
//  CellFrame
//
//  Created by mh on 16/4/1.
//  Copyright © 2016年 MH. All rights reserved.
//

#import "UIView+MHFrame.h"
#import <objc/runtime.h>

@implementation UIView (MHFrame)
////交换实例方法
//+ (void)exchangeInstanceMethod1:(SEL)method1 method2:(SEL)method2
//{
//    method_exchangeImplementations(class_getInstanceMethod(self, method1), class_getInstanceMethod(self, method2));
//}
////交换类方法
//+ (void)exchangeClassMethod1:(SEL)method1 method2:(SEL)method2
//{
//    method_exchangeImplementations(class_getClassMethod(self, method1), class_getClassMethod(self, method2));
//}

- (UIView *(^)(CGFloat))mh_x{
    return ^(CGFloat x){
        self.x = x;
        return self;
    };
}
- (void)setMh_x:(UIView *(^)(CGFloat))mh_x{}

- (UIView *(^)(CGFloat))mh_y{
    return ^(CGFloat y){
        self.y = y;
        return self;
    };
}
- (void)setMh_y:(UIView *(^)(CGFloat))mh_y{}

- (UIView *(^)(CGFloat))mh_w{
    return ^(CGFloat width){
        self.width = width;
        return self;
    };
}
- (void)setMh_w:(UIView *(^)(CGFloat))mh_w{}

- (UIView *(^)(CGFloat))mh_h{
    return ^(CGFloat height){
        self.height = height;
        return self;
    };
}
- (void)setMh_h:(UIView *(^)(CGFloat))mh_h{}

- (UIView *(^)(CGFloat))mh_centerX{
    return ^(CGFloat centerX){
        self.centerX = centerX;
        return self;
    };
}
- (void)setMh_centerX:(UIView *(^)(CGFloat))mh_centerX{}

- (UIView *(^)(CGFloat))mh_centerY{
    return ^(CGFloat centerY){
        self.centerY = centerY;
        return self;
    };
}
- (void)setMh_centerY:(UIView *(^)(CGFloat))mh_centerY{}

- (UIView *(^)(CGPoint))mh_origin{
    return ^(CGPoint origin){
        self.origin = origin;
        return self;
    };
}
- (void)setMh_origin:(UIView *(^)(CGPoint))mh_origin{}

- (UIView *(^)(CGSize))mh_size{
    return ^(CGSize size){
        self.size = size;
        return self;
    };
}
- (void)setMh_size:(UIView *(^)(CGSize))mh_size{}

/*************一下是结构体属性的便利使用******************************************************************/
- (void)setX:(CGFloat)x{
    CGRect frame = self.frame;
    frame.origin.x = x;
    self.frame = frame;
}
- (CGFloat)x{
    return self.frame.origin.x;
}

- (void)setY:(CGFloat)y{
    CGRect frame = self.frame;
    frame.origin.y = y;
    self.frame = frame;
}
- (CGFloat)y{
    return self.frame.origin.y;
}

- (void)setCenterX:(CGFloat)centerX{
    CGPoint center = self.center;
    center.x = centerX;
    self.center = center;
}
- (CGFloat)centerX{
    return self.center.x;
}

- (void)setCenterY:(CGFloat)centerY{
    CGPoint center = self.center;
    center.y = centerY;
    self.center = center;
}
- (CGFloat)centerY{
    return self.center.y;
}

- (void)setWidth:(CGFloat)width{
    CGRect frame = self.frame;
    frame.size.width = width;
    self.frame = frame;
}
- (CGFloat)width{
    return self.frame.size.width;
}

- (void)setHeight:(CGFloat)height{
    CGRect frame = self.frame;
    frame.size.height = height;
    self.frame = frame;
}
- (CGFloat)height{
    return self.frame.size.height;
}

- (void)setHalfWidth:(CGFloat)halfWidth{
    self.width = halfWidth * 2;
}

- (CGFloat)halfWidth{
    return self.width * 0.5;
}

- (void)setHalfHeight:(CGFloat)halfHeight{
    self.height = halfHeight * 2;
}

- (CGFloat)halfHeight{
    return self.height * 0.5;
}


- (void)setSize:(CGSize)size{
    CGRect frame = self.frame;
    frame.size = size;
    self.frame = frame;
}
- (CGSize)size{
    return self.frame.size;
}

- (void)setOrigin:(CGPoint)origin{
    CGRect frame = self.frame;
    frame.origin = origin;
    self.frame = frame;
}
- (CGPoint)origin{
    return self.frame.origin;
}
@end
