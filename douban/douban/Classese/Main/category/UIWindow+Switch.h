//
//  UIWindow+Switch.h
//  微博
//
//  Created by mh on 16/3/22.
//  Copyright © 2016年 mh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIWindow (Switch)

- (void)switchController;
@end
