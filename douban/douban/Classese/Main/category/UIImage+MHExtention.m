//
//  UIImage+MHExtention.m
//
//  Created by mh on 16/1/23.
//  Copyright © 2016年 mh. All rights reserved.
//

#import "UIImage+MHExtention.h"

@implementation UIImage (MHExtention)

+ (instancetype)resizableImageWithImageName:(NSString *)imagename{
    
    UIImage *image = [UIImage imageNamed:imagename];
    /**
     *  设置受保护的区域
     */
    CGFloat leftEdgeInset = image.size.width * 0.5;
    CGFloat topEdgeInset = image.size.height * 0.5;
    /**
     *  返回受保护的图片
     */
    return [image stretchableImageWithLeftCapWidth:leftEdgeInset topCapHeight:topEdgeInset];
   
}

@end
