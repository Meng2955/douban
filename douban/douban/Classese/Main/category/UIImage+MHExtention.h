//
//  UIImage+MHExtention.h
//
//  Created by mh on 16/1/23.
//  Copyright © 2016年 mh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (MHExtention)
/**
 *  通过传入一个图片名称,返回一张受保护的图片
 *  @param imagename 需要受保护的图片
 *  @return 返回受保护的图片
 */
+ (instancetype)resizableImageWithImageName:(NSString *)imagename;

@end
