//
//  UIBarButtonItem+Extension.h
//  微博
//
//  Created by mh on 16/3/22.
//  Copyright © 2016年 mh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIBarButtonItem (Extension)

+ (UIBarButtonItem *)itemWithTarget:(id)target action:(SEL)action image:(UIImage *)image highImage:(UIImage *)highImage;
+ (UIBarButtonItem *)itemWithTarget:(id)target action:(SEL)action image:(UIImage *)image seletedImage:(UIImage *)seletedImage;
@end