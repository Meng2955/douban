//
//  UIBarButtonItem+Extension.m
//  微博
//
//  Created by mh on 16/3/22.
//  Copyright © 2016年 mh. All rights reserved.
//

#import "UIBarButtonItem+Extension.h"

@implementation UIBarButtonItem (Extension)

+ (UIBarButtonItem *)itemWithTarget:(id)target action:(SEL)action image:(UIImage *)image highImage:(UIImage *)highImage{
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    // 设置图片
    if (image) [btn setBackgroundImage:image forState:UIControlStateNormal];
    if (highImage) [btn setBackgroundImage:highImage forState:UIControlStateHighlighted];
    // 设置尺寸
    btn.size = btn.currentBackgroundImage.size;
    UIView *view = [[UIView alloc] initWithFrame:btn.bounds];
    [view addSubview:btn];
    return [[UIBarButtonItem alloc] initWithCustomView:view];
}

+ (UIBarButtonItem *)itemWithTarget:(id)target action:(SEL)action image:(UIImage *)image seletedImage:(UIImage *)seletedImage{
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    // 设置图片
    if (image) [btn setBackgroundImage:image forState:UIControlStateNormal];
    if (seletedImage) [btn setBackgroundImage:seletedImage forState:UIControlStateSelected];
    // 设置尺寸
    btn.size = btn.currentBackgroundImage.size;
    //    UIView *view = [[UIView alloc] initWithFrame:btn.bounds];
    //    [view addSubview:btn];
    return [[UIBarButtonItem alloc] initWithCustomView:btn];
}
@end
