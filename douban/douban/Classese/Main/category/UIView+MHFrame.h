//
//  UIView+MHFrame.h
//  CellFrame
//
//  Created by mh on 16/4/1.
//  Copyright © 2016年 MH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (MHFrame)

//@property(strong)UIView *(^mh_margin)(CGFloat);

/** x */
@property(strong)UIView *(^mh_x)(CGFloat);
/** y */
@property(strong)UIView *(^mh_y)(CGFloat);
/** width */
@property(strong)UIView *(^mh_w)(CGFloat);
/** height */
@property(strong)UIView *(^mh_h)(CGFloat);
/** 中心点x坐标 */
@property(assign)UIView *(^mh_centerX)(CGFloat);
/** 中心点y坐标 */
@property(assign)UIView *(^mh_centerY)(CGFloat);
/**  Size  */
@property(assign)UIView *(^mh_size)(CGSize);
/** Origin  */
@property(assign)UIView *(^mh_origin)(CGPoint);


/** x坐标*/
@property(assign)CGFloat x;
/** y坐标 */
@property(assign)CGFloat y;
/**  宽度一半*/
@property(assign)CGFloat halfWidth;
/**  高度一半 */
@property(assign)CGFloat halfHeight;
/** 中心点x坐标 */
@property(assign)CGFloat centerX;
/** 中心点y坐标 */
@property(assign)CGFloat centerY;
/**  宽度  */
@property(assign)CGFloat width;
/**  高度  */
@property(assign)CGFloat height;
/**  Size  */
@property(assign)CGSize size;
/** Origin  */
@property(assign)CGPoint origin;
@end
